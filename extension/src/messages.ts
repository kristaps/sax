export const SNOOZE = "snooze"
export const LIST_DOMAIN = "list_domain"
export const UNLIST_DOMAIN = "unlist_domain"


export interface SnoozeMsg {
    type: typeof SNOOZE
    snoozeUntil: number
}

export interface ListDomainMsg {
    type: typeof LIST_DOMAIN
    domain: string
    timeAllowance: number  // seconds
}

export interface UnlistDomainMsg {
    type: typeof UNLIST_DOMAIN
    domain: string
}


export type Message = SnoozeMsg | ListDomainMsg | UnlistDomainMsg


export function broadcastTabMessage(message) {
    chrome.tabs.query(
        {},
        (tabs) => {
            for (let tab of tabs) {
                chrome.tabs.sendMessage(tab.id, message)
            }
        }
    )
}



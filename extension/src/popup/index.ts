import * as ReactDOM from 'react-dom'
import * as React from 'react'
import Popup from './views/Popup'


document.addEventListener('DOMContentLoaded', () => {
    ReactDOM.render(React.createElement(Popup), document.getElementById("root"))
})

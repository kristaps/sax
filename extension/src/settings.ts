/** Configuration handing */

import {SAX_GIFS} from './gifs'
import * as messages from './messages'
import {getTime} from './utils'

interface GlobalSettings {
    soundEnabled: boolean
    pausedUntil: number | null
    selectedGif: string
    selectedGifUrl: string
}

interface DomainSettings {
    domain: string
    timeAllowance: number | null  // seconds
    timeAllowanceMinutes: number | null
}

export interface CombinedSettings extends GlobalSettings, DomainSettings {}

interface loadSettingsCallback<SettingsT> { (settings: SettingsT): void }


export function loadSettings (domain: string, callback: loadSettingsCallback<CombinedSettings>) {
    let domainKey = "site." + domain
    let keys = ["soundEnabled", "pausedUntil", "selectedGif", domainKey]

    chrome.storage.local.get(
        keys,
        function (data) {
            let settings: CombinedSettings = {
                soundEnabled: true,
                pausedUntil: null,
                selectedGif: SAX_GIFS[0].label,
                selectedGifUrl: SAX_GIFS[0].url,
                domain,
                timeAllowance: null,
                timeAllowanceMinutes: null
            }

            if (data.soundEnabled != null) {
                settings.soundEnabled = data.soundEnabled
            }

            if (data.pausedUntil != null) {
                settings.pausedUntil = Number(data.pausedUntil)
                if (settings.pausedUntil <= getTime()) {
                    // Snooze expired
                    settings.pausedUntil = null
                }

                // TODO: add sanity check if snooze time was set in ms
            }

            let currentSelection = data.selectedGif
            if (currentSelection) {
                for (let i = 0; i < SAX_GIFS.length; i++) {
                    if (SAX_GIFS[i].label == currentSelection) {
                        settings.selectedGif = currentSelection
                        settings.selectedGifUrl = SAX_GIFS[i].url
                        break
                    }
                }
            }

            if (data[domainKey] != null) {
                settings.timeAllowanceMinutes = Number(data[domainKey])
                settings.timeAllowance = settings.timeAllowanceMinutes * 60
            }

            callback(settings)
        }
    )
}


/**
 * Set allowance time for a given domain
 * @param domain
 * @param minutes Allowance time in minutes. Passing null here unlists the domain
 */
export function setTimeAllowance(domain:string, minutes:number|null) {
    let domainKey = "site." + domain
    if (minutes == null) {
        chrome.storage.local.remove(domainKey)
        messages.broadcastTabMessage({type: messages.UNLIST_DOMAIN, domain})
    } else {
        let update = {}
        update[domainKey] = String(minutes)

        chrome.storage.local.set(update);
        messages.broadcastTabMessage(
            {type: messages.LIST_DOMAIN, domain, timeAllowance: minutes * 60}
        )
    }
}


export function setSnoozeTime(time) {
    let update = {pausedUntil: null}
    if (time != null) {
        let now = new Date().getTime() / 1000
        update.pausedUntil = String(now + time)
    }

    chrome.storage.local.set(
        update,
        () => {
            if (update.pausedUntil) {
                messages.broadcastTabMessage(
                    {type: messages.SNOOZE, snoozeUntil: update.pausedUntil}
                )
            }
        }
    )
}


export function setGif (id) {
    chrome.storage.local.set({selectedGif: id})
}


export function setSoundEnabled(enabled) {
    chrome.storage.local.set({soundEnabled: enabled})
}

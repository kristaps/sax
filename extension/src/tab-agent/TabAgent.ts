import { SAX_GIFS, RANDOM, getRandomGifUrl } from '../gifs'
import * as messages from '../messages'


interface Settings {
    soundEnabled: boolean
    pausedUntil: number
    time: number
    gifUrl: string
}

const MINUTE_SIZE = 60

const PROGRAM_TIMESTEP = 5
const PROGRAM = [
    {t: 0, side: "bottom", size: 30, position: 90, volume: 0.1},
    {t: 2, side: "top", size: 35, position: 10, volume: 0.2},
    {t: 4, side: "left", size: 30, position: 90, volume: 0.3},
    {t: 6, side: "right", size: 35, position: 10, volume: 0.4},
    {t: 8, side: "top", size: 50, position: 40, volume: 0.5},
    {t: 10, side: "bottom", size: 50, position: 60, volume: 0.6},
    {t: 30, side: "left", size: 50, position: 40, volume: 0.7},
    {t: 35, side: "right", size: 50, position: 60, volume: 0.9}
]

// Beware, this overrules any timings set above
for (let i = 0; i < PROGRAM.length; i++) {
    PROGRAM[i].t = PROGRAM_TIMESTEP * i
}


function getTime () {
    return new Date().getTime() / 1000
}


export default class TabAgent {
    domain:string = null
    audio = null
    programProgress = 0
    settings : Settings = null
    timeout = null
    spawnedGifs = []

    init () {
        let parts = new URL(String(window.location)).host.split('.')
        this.domain = parts.slice(parts.length - 2, parts.length).join('.')

        this.loadSettings((settings) => this.onSettingsLoaded(settings))
    }

    loadSettings (callback) {
        let domainKey = "site." + this.domain

        chrome.storage.local.get(
            [domainKey, "soundEnabled", "pausedUntil", "selectedGif"],
            function (settings) {
                if (settings.soundEnabled == null) {
                    settings.soundEnabled = true
                }

                if (settings.pausedUntil != null) {
                    settings.pausedUntil = Number(settings.pausedUntil)

                    // TODO: add sanity check if snooze time was set in ms
                }

                settings.time = null
                if (settings[domainKey] != null) {
                    // Convert to seconds
                    settings.time = Number(settings[domainKey]) * MINUTE_SIZE
                }

                let gifUrl = SAX_GIFS[0].url
                let currentSelection = settings.selectedGif

                if (currentSelection) {
                    if (currentSelection == RANDOM) {
                        gifUrl = getRandomGifUrl()
                    }
                    else {
                        for (let i = 0; i < SAX_GIFS.length; i++) {
                            if (SAX_GIFS[i].id == currentSelection
                                || SAX_GIFS[i].label == currentSelection  // support old settings
                            ) {
                                gifUrl = SAX_GIFS[i].url
                                break
                            }
                        }
                    }
                }
                settings.gifUrl = gifUrl

                callback(settings)
            }
        )
    }

    onSettingsLoaded (settings) {
        this.settings = settings
        this.initMessageListener()
        this.setAlarm()
    }

    onMessage (message: messages.Message) {
        switch (message.type) {
            case messages.SNOOZE:
                this.resetNags()
                this.settings.pausedUntil = message.snoozeUntil
                this.setAlarm()
                break

            case messages.LIST_DOMAIN:
                if (this.domain == message.domain) {
                    this.resetTabLoadTime()
                    this.settings.time = message.timeAllowance
                    this.setAlarm()
                }
                break

            case messages.UNLIST_DOMAIN:
                if (this.domain == message.domain) {
                    this.resetNags()
                    this.settings.time = null
                    this.setAlarm()
                }
                break
        }
    }

    initMessageListener () {
        chrome.runtime.onMessage.addListener(this.onMessage.bind(this))
    }

    /**
     * Sets timer to next time when something needs to happen.
     */
    setAlarm () {
        if (this.timeout) {
            window.clearTimeout(this.timeout)
            this.timeout = null
        }

        if (this.settings.time == null) {
            // Not a listed domain
            return
        }

        let now = getTime()
        let tabLoadTime = this.getTabLoadTime()

        if (this.settings.pausedUntil && this.settings.pausedUntil > now) {
            // Snooze in effect, wait until it expires
            this.timeout = window.setTimeout(
                () => {this.onWakeUp()},
                (this.settings.pausedUntil - now) * 1000
            )

        } else if (this.settings.time != null) {
            // The site is listed, wake when the time allowance is up or
            // when the next program step should be executed
            let partyTime = tabLoadTime + this.settings.time

            if (partyTime > now) {
                this.timeout = window.setTimeout(() => {this.onWakeUp()}, (partyTime - now) * 1000)

            } else {
                // wake on the next animation step
                let runningTime = now - partyTime
                if (this.programProgress < PROGRAM.length) {
                    let stepOffsetTime = PROGRAM[this.programProgress].t
                    this.timeout = window.setTimeout(
                        () => {this.onWakeUp()},
                        Math.max((stepOffsetTime - runningTime) * 1000, 0)
                    )
                }
            }
        }
    }

    onWakeUp () {
        this.advanceProgram()
        this.setAlarm()
    }

    advanceProgram () {
        let diff = getTime() - (this.getTabLoadTime() + this.settings.time)

        if (diff > 0) {
            if (this.settings.soundEnabled) {
                this.maybeInitAudio()
            }

            for (let i = this.programProgress; i < PROGRAM.length; i++) {
                let frame = PROGRAM[i]
                if (frame.t <= diff) {
                    this.spawnGif(frame.side, frame.size, frame.position)
                    this.setVolume(frame.volume)

                    if (i == 0) {
                        chrome.runtime.sendMessage({
                            type: "track_event",
                            eventCategory: "party-time",
                            eventAction: this.settings.gifUrl.split('/').pop()
                        })
                    }

                    this.programProgress = i+1
                }
            }
        }
    }

    maybeInitAudio () {
        if (!this.audio) {
            this.audio = this.spawnAudio()
        }
    }

    setVolume (volume) {
        if (this.audio) {
            this.audio.volume = volume
        }
    }

    getTabLoadTime () {
        let loadTime = getTime()
        let storedLoadTime = sessionStorage.getItem("loadTime")
        if (storedLoadTime) {
            loadTime = Number(storedLoadTime)
        } else {
            sessionStorage.setItem("loadTime", String(loadTime))
        }

        return loadTime
    }

    resetTabLoadTime () {
        sessionStorage.setItem("loadTime", String(getTime()))
    }

    spawnGif (side, size, position) {
        let body = document.getElementsByTagName("body")[0]

        let classes = (
            "sax-guy-helps-you-focus sax-" + side
        )
        let saxGuy = document.createElement("img");
        saxGuy.setAttribute("src", this.settings.gifUrl)

        saxGuy.setAttribute("class", classes)
        body.appendChild(saxGuy)
        this.spawnedGifs.push(saxGuy)

        let sizeRange = document.documentElement.clientHeight
        let posRange = document.documentElement.clientWidth
        if (side == "left" || side == "right") {
            [sizeRange, posRange] = [posRange, sizeRange]
        }

        saxGuy.addEventListener("load", function () {
            let imgHeight = Math.round(sizeRange / 100 * size)
            let imgWidth = Math.round(saxGuy.width * imgHeight / saxGuy.height)
            saxGuy.style.width = imgWidth + "px"
            saxGuy.style.height = imgHeight + "px"

            posRange = posRange - imgWidth

            let offset = Math.round(posRange / 100 * position)

            if (side == "left" || side == "right") {
                saxGuy.style.top = (offset - Math.round((imgHeight - imgWidth) / 2)) + "px"
            } else {
                saxGuy.style.left = offset + "px"
            }

            // set off-screen position
            saxGuy.style[side] = -imgHeight + "px"

            setTimeout(function () {
                let finalOffset = 0
                if (side == "left" || side == "right") {
                    finalOffset += Math.round((imgHeight - imgWidth) / 2)
                }
                saxGuy.style[side] = finalOffset + "px"
            })
        })
    }

    spawnAudio () {
        let saxLoop = document.createElement("audio")
        saxLoop.setAttribute("src", chrome.extension.getURL("media/sax-guy.ogg"))
        saxLoop.setAttribute("autoplay", "true")
        saxLoop.setAttribute("loop", "true")
        document.getElementsByTagName("body")[0].appendChild(saxLoop)

        return saxLoop
    }

    resetNags () {
        this.programProgress = 0

        if (this.audio) {
            this.audio.pause()
            this.audio.parentNode.removeChild(this.audio)
            this.audio = null
        }

        while (this.spawnedGifs.length) {
            let gif = this.spawnedGifs.pop()
            gif.parentNode.removeChild(gif)
        }
    }
}

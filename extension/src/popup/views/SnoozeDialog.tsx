import * as React from 'react'

const CHECK = "\u2713"

const TIME_CHOICES = [
    [60, '1 min'],
    [300, '5 mins'],
    [3600, '1 hour']
]

interface SnoozeDialogProps {
    requestClose: () => void
    setSnoozeTime: (number) => void
}
interface SnoozeDialogState {
    selectedTime: number|null
}

export default class SnoozeDialog extends React.Component<SnoozeDialogProps, SnoozeDialogState> {
    constructor () {
        super()

        this.state = {
            selectedTime: null
        }
    }

    onTimeChoiceClick = (time) => {
        this.setState({selectedTime: time})
        this.props.setSnoozeTime(time)
        window.setTimeout(this.props.requestClose, 600)
        ga('send', 'event', 'snooze')
    }

    render() {
        let timeChoices = TIME_CHOICES.map(
            (i) => <button onClick={() => this.onTimeChoiceClick(i[0])}>
                { this.state.selectedTime == i[0] ? `${CHECK} ${i[1]}` : i[1] }
            </button>
        )

        return <div className="pause-dialog">
            <div>Pause Sax for</div>
    
            <div className="time-choices">
                { timeChoices }
            </div>
    
            <button className="cancel" onClick={this.props.requestClose}>Cancel</button>
        </div>
    }
}

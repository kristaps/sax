import * as React from 'react'
import MainDialog from './MainDialog'
import SnoozeDialog from './SnoozeDialog'
import {loadSettings, CombinedSettings, setTimeAllowance} from '../../settings'
import * as settings from '../../settings'
import {getCurrentTabUrl, getDomain} from '../../utils'


const MAIN_DIALOG = "main_dialog"
const SNOOZE_DIALOG = "snooze_dialog"


interface PopupProps {}
interface PopupState {
    view: string
    settings: CombinedSettings | null
}

export default class Popup extends React.Component<PopupProps, PopupState> {
    state = {
        view: MAIN_DIALOG,
        settings: null
    }

    constructor () {
        super()

        getCurrentTabUrl((url) => loadSettings(getDomain(url), this.onSettingsLoaded))
    }

    onSettingsLoaded = (settings:CombinedSettings) => {
        this.setState({settings})
    }

    requestSnooze = () => {
        ga('send', 'pageview', '/snooze-options.html')
        this.setState({view: SNOOZE_DIALOG})
    }

    render() {
        if (!this.state.settings) {
            return null
        }

        switch (this.state.view) {
            case MAIN_DIALOG:
                return <MainDialog
                    settings={this.state.settings}
                    requestClose={window.close}
                    requestSnooze={this.requestSnooze}
                    setTimeAllowanceMinutes={
                        (minutes) => setTimeAllowance(this.state.settings.domain, minutes)}
                />

            case SNOOZE_DIALOG:
                return <SnoozeDialog
                    requestClose={window.close}
                    setSnoozeTime={settings.setSnoozeTime}
                />
        }
    }
}

const {CheckerPlugin} = require('awesome-typescript-loader')
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {

    entry: {
        popup: './src/popup/index.ts',
        options: './src/options/index.ts',
        "content-script": './src/tab-agent/index.ts'
    },

    output: {
        path: __dirname + "/build",
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },

    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'awesome-typescript-loader'
            }
        ]
    },
    plugins: [
        new CheckerPlugin(),
        new CopyWebpackPlugin([
            {from: 'src/media/', to: 'media/'},
            {from: 'src/manifest.json'},
            {from: 'src/*.html', flatten: true},
            {from: "src/*.css", flatten: true},
            {from: "src/init-ga.js"},
            {from: "src/background.js"}
        ])
    ]
};
import * as ReactDOM from 'react-dom'
import * as React from 'react'
import * as $ from 'jquery'
import { SAX_GIFS } from '../gifs'
import * as messages from '../messages'
import * as settings from '../settings'
import GifSelector from './views/GifSelector'


(function () {
    function renderDomainList (domains) {
        let $template = $(".domain-template").remove().find(".domain")
        let $container = $("#blocklist .content").empty()

        $.each(domains, function (durr, i) {
            let $item = $template.clone()
            $item.find(".name").text(i.domain)
            $item.find(".time").text(i.time)
            $item.find(".remove").click(function () {
                let domainKey = "site." + i.domain
                chrome.storage.local.remove(domainKey)
                $item.remove()
                messages.broadcastTabMessage({type: messages.UNLIST_DOMAIN, domain: i.domain})
                ga('send', 'event', 'unlist-site')
            })

            $container.append($item)
        })
    }

    chrome.storage.local.get(null, function (data) {
        let currentSelection = data.selectedGif
        if (!currentSelection) {
            currentSelection = SAX_GIFS[0].label
        }

        ReactDOM.render(
            React.createElement(
                GifSelector,
                {selectedGif: currentSelection, selectGif: settings.setGif}
            ),
            document.getElementById("gif-selection")
        )

        let domains = []
        for (let i in data) {
            if (i.substr(0, 5) == "site.") {
                domains.push({
                    domain: i.substr(5),
                    time: data[i]
                })
            }
        }

        if (domains.length > 0) {
            renderDomainList(domains)
        }

        let soundEnabled = data.soundEnabled
        if (soundEnabled === undefined) {
            soundEnabled = true
            settings.setSoundEnabled(soundEnabled)
        }

        $("#sound-switch").attr("checked", soundEnabled).change(function () {
            settings.setSoundEnabled(this.checked)
        })
    })
})()

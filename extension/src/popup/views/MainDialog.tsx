import * as React from 'react'
import {CombinedSettings} from "../../settings";

const CHECK = "\u2713"

interface MainDialogProps {
    settings: CombinedSettings
    setTimeAllowanceMinutes: (minutes: number|null) => void
    requestClose: () => void
    requestSnooze: () => void
}

interface MainDialogState {
    minutes: string
    saved: boolean
}


export default class MainDialog extends React.Component<MainDialogProps, MainDialogState> {

    constructor (props) {
        super()

        this.state = {
            minutes: String(props.settings.timeAllowanceMinutes || ''),
            saved: false
        }
    }

    onTimeInputKeyUp = (ev) => {
        if (ev.keyCode == 13) {
            this.setTimeAllowance()
        }
    }

    onTimeInputChange = (ev) => {
        this.setState({minutes: ev.target.value})
    }

    setTimeAllowance () {
        let value

        if (this.state.minutes.trim() == "") {
            value = null
        } else if (!isNaN(parseFloat(this.state.minutes))){
            value = parseFloat(this.state.minutes)
        } else {
            return
        }

        this.props.setTimeAllowanceMinutes(value)
        if (value != null) {
            ga('send', 'event', 'list-site')
        } else {
            ga('send', 'event', 'unlist-site')
        }

        this.setState(
            {saved: true},
            () => window.setTimeout(this.props.requestClose, 600)
        )
    }

    renderSnoozeInfo () {
        if (!this.isSnoozing()) {
            return null
        }

        return <div className="paused-info">
            Snoozing until {new Date(this.props.settings.pausedUntil*1000).toLocaleTimeString()}
        </div>
    }

    isSnoozing () {
        return this.props.settings.pausedUntil != null
    }

    render () {
        return <div className="main-dialog">
            <div className="content">
                <img src="media/icon128.png" width="64" height="64"/>

                <div id="config">
                    <div>
                        I don't want to browse
                    </div>

                    <div id="domain">{this.props.settings.domain}</div>

                    <div className="part3">
                        more than {" "}
                        <input type="number" id="time"
                            value={this.state.minutes}
                            onChange={this.onTimeInputChange}
                            onKeyUp={this.onTimeInputKeyUp}/>
                        mins
                    </div>
                </div>
            </div>

            <div id="actions">
                <button id="save" onClick={() => this.setTimeAllowance()}>
                    { this.state.saved ? CHECK + " Saved" : "Save" }
                </button>

                <button
                    id="snooze"
                    onClick={this.props.requestSnooze}
                    disabled={this.isSnoozing()}>Snooze</button>

                <button
                    id="settings"
                    onClick={() => chrome.tabs.create({'url': "/options.html" })}>Settings</button>
            </div>

            { this.renderSnoozeInfo() }
        </div>
    }
}

import * as React from 'react'
import {SAX_GIFS, RANDOM} from '../../gifs'


interface GifSelectorProps {
    selectedGif: string
    selectGif: (string) => void
}
interface GifSelectorState {
    selection: string
}

export default class GifSelector extends React.Component<GifSelectorProps, GifSelectorState> {
    constructor (props) {
        super(props)

        this.state = {selection: props.selectedGif}
    }

    onChange = (ev) => {
        this.setState({selection: ev.target.value})
        this.props.selectGif(ev.target.value)
    }

    render () {
        let options = []

        SAX_GIFS.concat([{'label': "Mystery GIF", id: RANDOM, url: ''}]).forEach(
            ({label, id}, i) => {
                let classNames = [`color${i % 2 + 1}`]
                // The label used to be used as the id, support existing settings
                let selected = label == this.state.selection || id == this.state.selection
                if (selected) classNames.push("selected")

                options.push(
                    <label key={label} className={classNames.join(' ')}>
                        <input
                            type="radio"
                            value={id}
                            checked={selected}
                            onChange={this.onChange}/>
                        <span className="">{label}</span>
                    </label>
                )
            }
        )

        return <div id="gif-selection">
            { options }
        </div>
    }
}

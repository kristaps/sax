export const RANDOM = 'random'

export const SAX_GIFS = [
    {
        id: "sax",
        label: "Sax",
        url: chrome.extension.getURL("media/sax-guy.gif")
    },
    {
        id: "nic",
        label: "Nic",
        url: "https://media.giphy.com/media/ImkoFVX25um5i/giphy.gif"
    },
    {
        id: "pickle",
        label: "Pickle",
        url: "https://media.giphy.com/media/3N5r5Ks7vo3f2/giphy.gif"
    },
    {
        id: "finger",
        label: "Finger",
        url: "https://media.giphy.com/media/QLkSBdMQqJN4c/giphy.gif"
    },
    {
        id: "gandalf",
        label: "Gandalf",
        url: "http://i3.kym-cdn.com/photos/images/original/001/187/255/5e9.gif"
    },
    {
        id: "doit",
        label: "DO IT!",
        url: "https://media.giphy.com/media/pGsfXogy3I2Xu/giphy.gif"
    }
]

export const RANDOM_GIFS = [
    // color toad
    "https://media.giphy.com/media/RtFfEQcEhD4By/giphy.gif",
    // blue hands
    "https://media.giphy.com/media/3og0IHq0M9MBeNVxMk/giphy.gif",
    // "mice"
    "https://media.giphy.com/media/3og0IGoStQwIfISP04/giphy.gif",
    // voxel pizza
    "https://media.giphy.com/media/prA6olJdnIDKg/giphy.gif",
    // waving skeleton
    "https://media.giphy.com/media/3owyplYLWlGFQk9mF2/giphy.gif",
    // running deer
    "https://media.giphy.com/media/xZzd7iz7UUGvS/giphy.gif",
    // dancing couple
    "https://media.giphy.com/media/VFTfD9eIc0gPC/giphy.gif",
    // unicorn
    "https://media.giphy.com/media/Q4Jv4BGIR0jZu/giphy.gif",
    // shuffle
    "https://media.giphy.com/media/GxssqT3bFwvcI/giphy.gif",
]


export function getRandomGifUrl () {
    let urls = SAX_GIFS.map((i) => i.url).concat(RANDOM_GIFS)
    return urls[Math.floor(urls.length * Math.random())]
}

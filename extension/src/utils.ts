interface getCurrentTabUrlCallback { (string): void }

export function getTime () {
    return new Date().getTime() / 1000
}

export function getCurrentTabUrl(callback: getCurrentTabUrlCallback) {
    chrome.tabs.query(
        { active: true, currentWindow: true },
        (tabs) => callback(tabs[0].url)
    )
}

export function getDomain (url) {
    let parts = new URL(url).host.split('.')
    return parts.slice(parts.length - 2, parts.length).join('.')
}

